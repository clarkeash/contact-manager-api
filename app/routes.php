<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

//Route::resource('contacts', 'ContactsController');

Route::group(array('prefix' => 'v1'), function()
{
	Route::group(array('prefix' => 'contacts'), function()
	{
		Route::get('/', function()
		{
			return Contact::all();
		});

		Route::get('{id}', function($id)
		{
			return Contact::find($id);
		});
	});
});