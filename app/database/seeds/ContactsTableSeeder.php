<?php

class ContactsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	// DB::table('contacts')->delete();

        $user1 = new Contact;
        $user1->name = "Ashley Clarke";
        $user1->email = "me@ashleyclarke.me";
        $user1->twitter = "clarkeash";
        $user1->save();

        $user2 = new Contact;
        $user2->name = "John Doe";
        $user2->email = "john@ashleyclarke.me";
        $user2->twitter = "john_fake";
        $user2->save();

        $user3 = new Contact;
        $user3->name = "Fred Bloggs";
        $user3->email = "fred@ashleyclarke.me";
        $user3->twitter = "freddie";
        $user3->save();

        $user4 = new Contact;
        $user4->name = "Jane Smith";
        $user4->email = "jane@ashleyclarke.me";
        $user4->twitter = "jane";
        $user4->save();

        $user5 = new Contact;
        $user5->name = "Frank Bruno";
        $user5->email = "frank@ashleyclarke.me";
        $user5->twitter = "frankieLad";
        $user5->save();


        // Uncomment the below to run the seeder
        // DB::table('contacts')->insert($contacts);
    }

}